﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using System.IO;

namespace SendShipmentEmail
{
    class Program
    {
        public static Dictionary<string, string> rts = new Dictionary<string, string>();


        static void Main(string[] args)
        {
            using (StreamWriter writ = new StreamWriter(@"\\wcmktg.com\WCPG-HQ\Public\edi\exe\SendShipmentEmailErrors\" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".txt", true))
            {
                writ.WriteLine(String.Join("|",args));
            }
            string deliverynumber = args[1];
            //string deliverynumber = "0080000119";
            string connectTo = args[0];
            //string connectTo = "DEV";
            SetRoutes();
            SendEmail(deliverynumber,connectTo);
        }
        public static void SendEmail(string deliverynumber, string connectTo)
        {
            Dictionary<string, string> client = new Dictionary<string, string>();
            client.Add("DEV", "110");
            client.Add("PRD", "100");
            client.Add("QAS", "100");
            string q = "Select znd.MANDT as client, znd.VBELN as Delivery, znd.POSNR as DeliveryItem, zndhead.KUNWE as shipTo, po.BSTNK as PONumber, znd.MATNR as MaterialNum, znd.LFIMG as ActualQtyShipped, " +
                " realuom.MSEH3 as UOM, znd.KWMENG as originalqty, zndhead.LINE0 as addr0, zndhead.LINE1 as addr1, zndhead.LINE2 as addr2, zndhead.LINE3 as addr3, zndhead.LINE4 as addr4, zndhead.LINE5 as addr5, zndhead.LINE6 as addr6, " +
                " zndhead.LINE7 as addr7, zndhead.LINE8 as addr8, zndhead.LINE9 as addr9, znd.ARKTX as description, zndhead.TRACKING as tracknum, zndhead.ROUTE as provider " +
                ", zn.EMAIL as email " +
                        " from "+connectTo.ToLower()+".ZNOTIFYDELH as zndhead " +
                        " inner join " + connectTo.ToLower() + ".ZNOTIFYDEL as znd on znd.VBELN = zndhead.VBELN" +
                                      " inner join "+connectTo.ToLower()+".ZNOTIFY as zn on zn.VBELN = zndhead.SALESORDER " +
                        "INNER JOIN " + connectTo.ToLower() + ".T006A as realuom ON zndhead.MANDT = realuom.MANDT AND znd.VRKME = realuom.MSEHI " + 
                        "inner join " + connectTo.ToLower() + ".VBAK as po on zndhead.MANDT=po.MANDT and zndhead.SALESORDER=po.VBELN "+
                        " where zndhead.VBELN=@deliverynumber and zndhead.MANDT=" + client[connectTo]+" and znd.MANDT="+client[connectTo]+" " +
                        " and zn.MANDT=" + client[connectTo] + " and realuom.SPRAS = 'E'";
            try
            {
              //  Console.WriteLine(connectTo);
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPConnection" + connectTo].ConnectionString);

                //Console.WriteLine("Connection Created");
                conn.Open();
                //Console.WriteLine("Connection Open");
                SqlCommand cmd = new SqlCommand(q, conn);
                //Console.WriteLine("Command Created");
                cmd.Parameters.Add(new SqlParameter("@deliverynumber", System.Data.SqlDbType.VarChar));
                cmd.Parameters["@deliverynumber"].Value = deliverynumber;
                //Console.WriteLine("Command Compiled");
                SqlDataReader reader = cmd.ExecuteReader();
                //Console.WriteLine("Command Executed");
                if (reader.HasRows)
                {
                  //  Console.WriteLine("Have Lines");
                    reader.Read();

                    string shipToName = reader["addr0"].ToString();
                    string OrderNumber = reader["PONumber"].ToString();
                    string mailaddr = "";
                    string provider = reader["provider"].ToString();
                    string tracknum = reader["tracknum"].ToString();
                    string address = "";

                    for (int x = 0; x < 10; x++)
                    {
                        if (reader["addr" + x.ToString()].ToString() != " ")
                        {
                            mailaddr += reader["addr" + x.ToString()].ToString() + "<br />";
                        }
                    }
                    if (provider == "CUS-UP" || provider == "UPS100" || provider == "WCHUF" || provider == "WCHUP1" || provider == "WCHUP2" ||
                               provider == "WCHUP3" || provider == "WCHUP4" || provider == "WCHUP5" || provider == "WCHUP6" || provider == "WCHUP7" ||
                                provider == "WCHUPF" || provider == "WCHUPS")
                    {
                        address = "https://www.ups.com/track?loc=en_US&tracknum=" + tracknum;
                    }
                    else if (provider == "CUS-FE" || provider == "CUS-FN" || provider == "CUS-FS" || provider == "CUS-FX")
                    {
                        address = "https://www.fedex.com/apps/fedextrack/?tracknumbers=" + tracknum;
                    }


                    string message = "";
                    SmtpClient smtp = new SmtpClient("192.168.100.18", 25);
                    string defaultRecipient = reader["email"].ToString();
                    //string defaultRecipient = "mmoore@westchestergear.com";
                    MailMessage email = new MailMessage();
                    email.Attachments.Add(new Attachment("\\\\wcmktg.com\\NETLOGON\\EmailImages\\emaillogo.png")
                    {
                        ContentId = "logo"
                    });
                    email.To.Add(defaultRecipient);
                    email.To.Add("mmoore@westchestergear.com");
                    email.Subject = "Your order has shipped";
                    email.From = new MailAddress("Shipping@westchestergear.com");
                    message += "<div style='background-color:black;padding:5px;'><img src='cid:logo' height='75' /></div>";
                    message += "Dear " + shipToName;
                    message += "<br /><br />";
                    message += "<div style='font-weight:bold;'>Order Number:  " + OrderNumber + "</div>";
                    message += "Thank you for your order from <b>West Chester Protective Gear!</b>  We wanted to let you know that your order was shipped via, " + rts[provider];
                    message += " on " + DateTime.Now.ToString("MM/dd/yyyy") + ".";
                    message += "<br />";
                    message += "You can track your package(s) at any time using the link below, <b><u>it may take 24-48 hours for tracking information to be updated!</u></b><br /><br />";
                    message += "<div style='font-weight:bold;'>Shipped To:</div>";
                    message += "<div>" + mailaddr + "</div>";
                    message += "<br />";
                    message += "<div style='font-weight:bold;'>Track Your Shipment:  ";
                    if (address != "")
                    {
                        message += "<a href='" + address + "'>" + tracknum + "</a></div>";
                    }
                    else
                    {
                        message += tracknum + "</div>";
                    }
                    message += "<br /><br />";
                    message += "This shipment includes the following items: <br />";
                    message += "<table><tr style='border-bottom:1px solid black;'><th width='10%'>Item #</th><th width='60%'>Description</th><th width='15%'>Qty shipped</th><th width='15%'>Qty Ordered</th></tr>";

                    do
                    {
                        try
                        {
                            message += "<tr><td width='10%'>" + reader["MaterialNum"].ToString() + "</td><td width='60%' style='text-align:center;'>" + reader["description"].ToString() + "</td><td width='15%'>" + ((int)(Double.Parse(reader["ActualQtyShipped"].ToString()))).ToString() + " " + reader["UOM"].ToString() + "</td><td width='15%'>" + ((int)(Double.Parse(reader["originalqty"].ToString()))).ToString() + " " + reader["UOM"].ToString() + "</td></tr>";
                        }
                        catch (Exception e)
                        {
                            message += "<tr><td width='10%'>" + reader["MaterialNum"].ToString() + "</td><td width='60%' style='text-align:center;'>" + reader["description"].ToString() + "</td><td width='15%'>" + reader["ActualQtyShipped"].ToString() + " " + reader["UOM"].ToString() + "</td><td width='15%'>" + reader["originalqty"].ToString() + " " + reader["UOM"].ToString() + "</td></tr>";
                        }
                    } while (reader.Read());


                    message += "</table><br /><br />";
                    message += "<div style='font-weight:bold;'>Thank you for your business and we look forward to serving you in the future!</div>";




                    email.Body = message;
                    email.IsBodyHtml = true;
                    smtp.Send(email);
                } else
                {
                    using (StreamWriter writ = new StreamWriter(@"\\wcmktg.com\WCPG-HQ\Public\edi\exe\SendShipmentEmailErrors\" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".txt", true))
                    {
                        writ.WriteLine(cmd.CommandText);
                    }
                }
                conn.Close();
            } catch (Exception e)
            {
                using (StreamWriter writ = new StreamWriter(@"\\wcmktg.com\WCPG-HQ\Public\edi\exe\SendShipmentEmailErrors\" + DateTime.Now.ToShortDateString().Replace("/", "-") + ".txt", true))
                {
                    writ.WriteLine(e.Message);
                    //writ.WriteLine(e.InnerException);
                    writ.WriteLine(e.Source);
                    writ.WriteLine(e.StackTrace);
                    writ.WriteLine(e.TargetSite);
                   // writ.WriteLine(e.ToString());
                }
            }
        }
        public static void SetRoutes()
        {
            rts.Add("1TIME", "One Time");
            rts.Add("CUS-AA", "AAA Cooper");
            rts.Add("CUS-AB", "Aberdeen");
            rts.Add("CUS-AF", "ABF");
            rts.Add("CUS-AI", "Airborne");
            rts.Add("CUS-AL", "Alvan");
            rts.Add("CUS-AS", "All States");
            rts.Add("CUS-AV", "Averitt");
            rts.Add("CUS-BB", "Best Freight");
            rts.Add("CUS-BX", "Bax Global");
            rts.Add("CUS-CC", "Cross County");
            rts.Add("CUS-CT", "Central");
            rts.Add("CUS-CX", "Con-Way");
            rts.Add("CUS-DF", "Dayton Freight");
            rts.Add("CUS-DH", "DHL");
            rts.Add("CUS-DU", "Dugan");
            rts.Add("CUS-DW", "Dawes");
            rts.Add("CUS-EF", "Estes");
            rts.Add("CUS-ES", "Estes");
            rts.Add("CUS-FE", "FedEx");
            rts.Add("CUS-FN", "FedEx National");
            rts.Add("CUS-FS", "FedEx SmartPost");
            rts.Add("CUS-FX", "FedEx(Small)");
            rts.Add("CUS-HG", "Hub Group");
            rts.Add("CUS-HL", "Holland");
            rts.Add("CUS-JB", "JB Hunt");
            rts.Add("CUS-JJ", "J&J");
            rts.Add("CUS-KG", "King");
            rts.Add("CUS-MA", "US Postal Service");
            rts.Add("CUS-ML", "Milan");
            rts.Add("CUS-MM", "M&M");
            rts.Add("CUS-MS", "Mid State");
            rts.Add("CUS-MU", "Multi Serve");
            rts.Add("CUS-NE", "NEMF");
            rts.Add("CUS-OV", "Overnite");
            rts.Add("CUS-PJ", "P-Jax");
            rts.Add("CUS-PO", "Pitt Ohio");
            rts.Add("CUS-PS", "Pitt Ohio Small Pack");
            rts.Add("CUS-RH", "Rush");
            rts.Add("CUS-RL", "R&L");
            rts.Add("CUS-RR", "Roadrunner");
            rts.Add("CUS-RW", "YRC");
            rts.Add("CUS-RX", "YRC Guaranteed");
            rts.Add("CUS-SA", "SAIA");
            rts.Add("CUS-SW", "Swift");
            rts.Add("CUS-TS", "Transaver");
            rts.Add("CUS-UC", "U.S.Cargo");
            rts.Add("CUS-UP", "UPS");
            rts.Add("CUS-UX", "US Express");
            rts.Add("CUS-VK", "Vork");
            rts.Add("CUS-VT", "Vitran");
            rts.Add("CUS-WC", "Customer WILL CALL");
            rts.Add("CUS-WD", "Ward");
            rts.Add("CUS-YW", "YRC Expedited");
            rts.Add("EXPORT", "Export from Monroe");
            rts.Add("TBD", "To Be Determined");
            rts.Add("UPS100", "UPS Ground - Small Parcel");
            rts.Add("WCHOWN", "WCH Own Vehicle !");
            rts.Add("WCHUF", "UPS Freight");
            rts.Add("WCHUP1", "UPS Next Day Early AM");
            rts.Add("WCHUP2", "UPS Next Day");
            rts.Add("WCHUP3", "Next Day Air Saver");
            rts.Add("WCHUP4", "Next Day Air Letter");
            rts.Add("WCHUP5", "2nd Day Air AM");
            rts.Add("WCHUP6", "2nd Day Air");
            rts.Add("WCHUP7", "Three Day Select");
            rts.Add("WCHUPF", "UPS Freight LTL");
            rts.Add("WCHUPS", "UPS Ground");
        }
    }
}
